# Nextcloud
Elukerio propose aux adhérents une solution "Cloud".

Cette dernière permet le stockage et la synchronisation de fichiers, de contacts, d'agendas et de notes entre les appareils.

Elle permet également la création de formulaires/sondages publics et le partage de fichier.

## Se connecter au service
Tout adhérent dont la cotisation est à jour peut se connecter au cloud à l'adresse [https://cloud.elukerio.org](https://cloud.elukerio.org).

## Dossiers partagés
Sur votre espace vous trouverez des dossiers partagés en fonction de votre rôle au sein de l'association.
Tout adhérent dispose par défaut d'un droit de lecture sur le dossier "Membres" contenant tous les documents à disposition des adhérents (PV des AG, états de comptes, statuts de l'association, etc.).

## Limite de l'espace de stockage
Tout adhérent à le droit à 1Go pour une adhésion standard.
Il est possible d'augmenter ce quota dans le cas d'une adhésion soutien.

## Installation du logiciel de synchronisation "Nextcloud-Desktop"
Le logiciel de synchronisation permet de synchroniser automatiquement certains dossiers de votre ordinateur/smartphone avec le cloud Elukerio.
Toutes les instructions se trouvent à cette adresse: [https://nextcloud.com/install/#install-clients](https://nextcloud.com/install/#install-clients).
