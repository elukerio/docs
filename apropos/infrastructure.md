# Notre infrastructure

Nous sommes hébergés chez [Rezopole](https://www.rezopole.net/fr/) sur un serveur dédié gracieusement cédé par [Alolise](https://alolise.org/).
Coté logiciel, nous utilisons Proxmox et nos services tournent dans des conteneurs LXC.

Les services Web sont accessibles à travers un proxy Nginx qui redirige les requêtes de l'utilisateur sur le serveur web interne au conteneur LXC du service.
Un schéma sera bientôt disponible...