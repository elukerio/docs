# Auto-hébergement

Cette page est vide pour le moment.
Cependant voici quelques pistes pour vous aider.

Un wiki général sur le sujet (Choix du serveur, FAI, etc.) pour s'héberger à la maison: [http://wiki.auto-hebergement.fr/](http://wiki.auto-hebergement.fr/).

Le wiki de Framasoft avec des guides complets pour installer de nombreux services: [https://framacloud.org/fr/cultiver-son-jardin/](https://framacloud.org/fr/cultiver-son-jardin/).