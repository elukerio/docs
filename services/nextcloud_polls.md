# Création de sondages (alternative à Doodle)
Elukerio met à disposition un outil permettant de créer facilement des sondages sur son cloud à l'addresse suivante : [https://cloud.elukerio.org/apps/polls/](https://cloud.elukerio.org/apps/polls/).

Vous pourrez par exemple trouver la meilleure date pour l'organisation d'un évènement en interrogeant les participant(e)s.

Plus d'informations sur la connexion au cloud sur [cette page](/services/nextcloud.html).

