# Création de formulaires (alternative à Google Forms) 

L'outil est accessible sur le cloud d'Elukerio à cette adresse: [https://cloud.elukerio.org/apps/forms/](https://cloud.elukerio.org/apps/forms/).

Plus d'informations sur la connexion au cloud sur [cette page](/services/nextcloud.html).
