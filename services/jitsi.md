# Jitsi
Jitsi-meet est outil de visioconférence depuis son navigateur web à l'adresse : [jitsi.elukerio.org](https://jitsi.elukerio.org).

## Initier une visioconférence
Rendez vous sur [jitsi.elukerio.org](https://jitsi.elukerio.org), le logiciel vous proposera de choisir un nom de "salon" que vous devrez partager avec les autres participants.

Vous pouvez aussi directement saisir une adresse de ce type : https://jitsi.elukerio.org/Nom-Du-Salon-Choisit
